section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, rax,
    mov rax, 60
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    ret
    .loop:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    call string_length
    mov rdx, rax 
    
    mov rsi, rdi 
    mov rax, 1 
    mov rdi, 1 

    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi 

    mov rax, 1 
    mov rdi, 1 
    mov rsi, rsp 
    mov rdx, 1 

    syscall

    pop rdi 
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
     mov r8, 10
    xor rcx, rcx
    mov rax, rdi
    .loop:
        xor rdx, rdx
        div r8
        push rdx
        inc rcx
        cmp rax, 0
        jnz .loop

    .rett:
        pop rdi
        add rdi, 0x30
        push rcx
        call print_char
        pop rcx
        dec rcx
        cmp rcx, 0
        jnz .rett
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    ret

    mov rax, rdi
    mov r9, 0x8000000000000000
    test rax, r9
    jnz .under
    .above:
        call print_uint
        ret
    .under:
        mov rsi, rdi
        mov rdi, 0x2D
        push rsi
        call print_char
        pop rsi
        mov rdi, rsi
        neg rdi
        call print_uint
        ret
    

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    ret

    xor rax, rax; for loop
    .count:
      mov dh, [rdi+rax]
      mov dl, [rsi+rax]
      cmp dl, dh
      jne .different
      cmp dl, 0
      je .equals
      inc rax
      jmp .count
    .different:
        xor rax,rax
        ret
    .equals:
        mov rax,1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    mov rax, SYSCALL_READ
    mov rdi, STDIN
    push 0; make a space in memory
    mov rsi, rsp; to the free space in memory
    mov rdx, 1; we need only 1 char
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    .whitespace_loop: ;skip whitespaces
        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi

        cmp al, 0x20
        je .whitespace_loop
        cmp al, 0x09
        je .whitespace_loop
        cmp al, 0x0A
        je .whitespace_loop

    ; rax - first symbol of the word
    xor r8, r8
    .loop: ;read symbols
        cmp rax, 0
        je .end_of_loop
        cmp rax, 0x20
        je .end_of_loop
        cmp rax, 0x09
        je .end_of_loop
        cmp rax, 0x0A
        je .end_of_loop
        mov byte[rdi+r8], al
        inc r8
        cmp r8, rsi
        jge .buffer_overflow

        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi

        jmp .loop
    .end_of_loop:
        mov byte[rdi+r8], 0

    jmp .end
    .buffer_overflow:
        xor rdi, rdi
    
    .end:
        mov rax, rdi
        mov rdx, r8
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    ret

    xor rax, rax
        xor rsi, rsi
        xor r9, r9
        mov r8, 10
    .loop:
            mov sil, [rdi+r9]
            cmp sil, null
            je .end
            cmp sil, zero_symbol
            jb .end
            cmp sil, nine_symbol
            ja .end
            mul r8
            sub sil, zero_symbol
            add rax, rsi
            inc r9
            jmp .loop
    .end:
            mov rdx, r9
            ret
    




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    ret 
    mov al, byte[rdi]
	cmp al, "-"
	jne .positive
	inc rdi
	call parse_uint
	neg rax
	inc rdx
	ret
	.positive:
		call parse_uint
		ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    ret

    call string_length	; get string length
	cmp rax, rdx		; compare length of string and buffer size
	jae .different_length	; if they are different stop copy
	mov rax, 0
	
	.loop:
		mov cl, byte[rdi+rax]	; move string char to cl
		mov byte[rsi+rax], cl	; move cl shar to buffer
		inc rax			; inc counter
		cmp cl, 0		; if cl is 0 then it means that it's the end of string
		jne .loop		; else loop again
		ret

	.different_length:
		mov rax, 0
		ret	
